<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Product; 
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Component
{

    use WithPagination;
    use WithFileUploads;

    public $name, $barcode, $cost, $price, $stock, $alerts, $category_id, $search, $image, $selected_id, $pageTitle, $componentName;

    private $pagination = 5;

    public function paginationView(){

        return 'vendor.livewire.bootstrap';
    }

     public function mount(){
        $this->pageTitle = 'Listado';
        $this->componentName = 'Productos';
        $this->categoryid = 'Elegir';
     }

    public function render()
    {

        if(strlen($this->search) > 0){
            $products = Product::join('categories as c', 'c.id', 'products.category_id')
                        ->select('products.*', 'c.name as category')
                        ->where('products.name', 'like', '%'. $this->search. '%')
                        ->orWhere('products.barcode','like', '%'.$this->search.'%')
                        ->orWhere('c.name','like', '%'.$this->search.'%')
                        ->orderBy('products.name', 'desc')
                        ->paginate($this->pagination);
        }else{
            $products = Product::join('categories as c', 'c.id', 'products.category_id')
                        ->select('products.*', 'c.name as category')
                        ->orderBy('products.id', 'desc')
                        ->paginate($this->pagination);
        }
        return view('livewire.products.products',[
            'data' => $products, 
            'categories' => Category::orderBy('name', 'asc')->get()
        ])->extends('layouts.theme.app')->section('content');
    }


/*-------------------------Metodo para guardar producto------------------------------------------*/

    public function Store(){

        //Definimos reglas de validacion
        $rules = [
            'name' => 'required|unique:products|min:3',
            'barcode' => 'required|unique:products',
            'cost' => 'required',
            'price'=>'required',
            'stock'=>'required',
            'alerts'=>'required',
            'categoryid'=>'required|not_in:Elegir'
        ];

        //Definimos mensajes de la validacion
         $messages = ['name.required'=>'Nombre del producto es requerido',
                     'name.unique'=>'Ya existe el nombre del producto',
                     'name.min'=>'El nombre del producto debe tener minimo 3 caracteres',
                     'barcode.unique'=>'El codigo del producto ya existe',
                     'cost.required' =>'El costo del producto es requerido',
                     'price.required'=>'El precio del producto es requerido',
                     'stock.required' => 'El stock del producto es requerido',
                     'alerts.required'=>'El stock minimo del producto es requerido',
                     'categoryid.required'=>'La categoria del producto es requerido'
                    ];

        $this->validate($rules, $messages);


        //Creamos consulta
        $product = Product::create([
            'name' => $this->name,
            'barcode'=>$this->barcode,
            'cost' => $this->cost,
            'price' => $this->price,
            'stock' => $this->stock,
            'alerts' => $this->alerts,
            'category_id' => $this->categoryid
        ]);

        //Variable de nombre de la imagen
        $customFileName;
        if($this->image){

            $customFileName = uniqid().'_.'.$this->image->extension();
            $this->image->storeAs('public/products',$customFileName);
            $product->image = $customFileName;
            $product->save();
        }

        //Reseteamos campos
        $this->resetUI();
        $this->emit('product-added', 'Producto registrado');


    }

 /*------------------------------------------------Fin Metodo---------------------------------------*/




/*---------------------------------------------Metodo para editar producto-------------------------*/

public function Edit($id){

    $record = Product::find($id, ['id','name','barcode','cost','price','stock', 'alerts', 'category_id', 'image']);

    $this->selected_id = $record->id;
    $this->name = $record->name;
    $this->barcode = $record->barcode;
    $this->cost = $record->cost;
    $this->price = $record->price;
    $this->stock = $record->stock;
    $this->alerts = $record->alerts;
    $this->categoryid = $record->category_id;
    $this->image = null;
    $this->emit('show-modal', 'show-modal');

}


public function Update(){

    $rules = [
        'name' => "required|min:3",
        'barcode' => "required|unique:products,barcode,{$this->selected_id}",
        'cost' => 'required',
        'price'=>'required',
        'stock'=>'required',
        'alerts'=>'required',
        'categoryid'=>'required'
    ];

    $messages = [
        'name.required'=>'El nombre del producto es requerido',
        'name.min'=>'El nombre del producto debe tener minimo 3 caracteres',
        'barcode.required'=>'El codigo del producto es requerido',
        'barcode.unique'=>'El codigo del producto ya existe',
        'cost.required'=>'El costo del producto es requerido',
        'price.required'=>'El precio del producto es requerido',
        'stock.required'=>'El stock del producto es requerido',
        'alerts.required'=>'El stock minimo es requerido',
        'categoryid.required'=>'La categoria del producto es requerido'
    ];


    $this->validate($rules, $messages);

    $product = Product::find($this->selected_id);

    $product->update([
        'name'=> $this->name,
        'barcode'=> $this->barcode,
        'cost'=> $this->cost,
        'price'=> $this->price,
        'stock'=> $this->stock,
        'alerts'=> $this->alerts,
        'category_id'=> $this->categoryid
    ]);


    if($this->image){
        $customFileName = uniqid().'_.'.$this->image->extension();

        $this->image->storeAs('public/products', $customFileName);
        $imageName = $product->image;

        $product->image = $customFileName;
        $product->save();

        if($imageName != null){
            if(file_exists('storage/products' . $imageName)){
                    unlink('storage/products' . $imageName);
                }
        }
    }

    $this->resetUI();
    $this->emit('product-updated', 'Producto actualizado');

}


/*------------------------------------------------Fin Metodo---------------------------------------*/



/*-------------------------------------------------Metodo para eliminar registro---------------------*/

protected $listeners = [
    'deleteRow' => 'Destroy'
];

public function Destroy(Product $product){

    $imageName = $product->image; //Nombre de la imagen temporal

    $product->delete();

    if($imageName != null){
        unlink('storage/products/'. $imageName);
    }

    $this->resetUI();
    $this->emit('product-deleted', 'Producto eliminado');
    

}


/*------------------------------------------------Fin Metodo---------------------------------------*/



    public function resetUI(){
        $this->name = '';
        $this->barcode = '';
        $this->cost = '';
        $this->price = '';
        $this->stock = '';
        $this->categoryid = 'Elegir';
        $this->image = null;
        $this->search = '';
        $this->selected_id = 0;
        $this->alerts = '';

        $this->emit('modal-hide', 'modal-hide');
    }

}
