<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category; //Llamamos modelo de categorias
use Livewire\WithFileUploads; //Trait para subir imagenes
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Component
{

    use WithFileUploads;
    use WithPagination;

    public $name, $search, $image, $selected_id, $pageTitle, $componentName;
    private $pagination = 5;

    //Inicializamos montando la informacion
    public function mount(){

        $this->pageTitle = 'Listado';
        $this->componentName = 'Categorias';
        $this->selected_id = 0;
    }

    //Metodo para la paginacion
    public function paginationView(){
        return 'vendor.livewire.bootstrap';
    }

    public function render()
    {
        if(strlen($this->search) > 0){

             $data = Category::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagination);
        }else{
            $data = Category::orderBy('id', 'desc')->paginate($this->pagination);
        }
       
        return view('livewire.category.categories', ['categories' => $data])
        ->extends('layouts.theme.app')
        ->section('content');

    }


    //Metodo para mostrar modal de categoria para editar
    public function Edit($id){

        $record = Category::find($id, ['id', 'name', 'image']);

        $this->name = $record->name;
        $this->selected_id = $record->id;
        $this->image = null;
        $this->emit('show-modal', 'show modal');
    }


    //Metodo para guardar cateogrias
    public function Store(){

        $rules = ['name'=>'required|unique:categories|min:3'];
        
        $messages = ['name.required'=>'Nombre de la categorias es requerido',
                     'name.unique'=>'Ya existe el nombre de la categoria',
                     'name.min'=>'El nombre de la categoria debe tener minimo 3 caracteres'
                    ];
        $this->validate($rules, $messages);


        $category = Category::create([
            'name' => $this->name
        ]);

        $customFileName;
        if($this->image){

            $customFileName = uniqid().'_.'.$this->image->extension();
            $this->image->storeAs('public/categories', $customFileName);
            $category->image = $customFileName;
            $category->save();
        }

        $this->resetUI();
        $this->emit('category-added', 'Categoria registrada');
    }



    //Metodo para actualizar informacion
    public function Update(){

        $rules = ['name'=>"required|min:3|unique:categories,name,{$this->selected_id}"];

        $messages = ['name.required'=>'Nombre de categoria requerido',
                     'name.min' => 'El nombre de la categoria debe tener minimo 3 caracteres',
                     'name.unique' => 'El nombre de la categoria ya existe'];

        $this->validate($rules, $messages);

        $category = Category::find($this->selected_id);
        $category->update([
            'name' => $this->name
        ]);

        if($this->image){
            $customFileName = uniqid().'_.'.$this->image->extension();
            $this->image->storeAs('public/categories', $customFileName);
            $imageName = $category->image;

            $category->image = $customFileName;
            $category->save();

            if($imageName != null){

                if(file_exists('storage/categories' . $imageName)){
                    unlink('storage/categories' . $imageName);
                }

            }
        }

        $this->resetUI();
        $this->emit('category-updated', 'Cateogria actualizada');

    }

    protected $listeners = [
        'deleteRow' => 'Destroy'
    ];

    //Metodo para eliminar categorias
    public function Destroy(Category $category){

        //$category = Category::find($id);
        //dd($category);
        $imageName = $category->image; //Imagen temporal
        $category->delete();

        if($imageName != null){
            unlink('storage/categories/'. $imageName);
        }

        $this->resetUI();
        $this->emit('category-deleted', 'Categoria eliminada');

    }

    //Limpiamos proiedades publicas
    public function resetUI(){
        $this->name = '';
        $this->image = null;
        $this->search = '';
        $this->selected_id = 0;
        $this->emit('modal-hide', 'modal-hide');
    }
}
