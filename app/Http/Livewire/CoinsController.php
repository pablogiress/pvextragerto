<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Denomination;
use Livewire\WithFileUploads; //Trait para subir imagenes
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;

class CoinsController extends Component
{

    use WithFileUploads;
    use WithPagination;

    public $type, $value, $image, $pageTitle, $componentName, $selected_id, $search;
    private $pagination = 5;

    public function render()
    {

        return view('livewire.denominations.coins',['coins' => Denomination::paginate(5)
            ])->extends('layouts.theme.app')->section('content');
    }


    public function mount(){

        $this->pageTitle = 'Listado';
        $this->componentName = 'Monedas';
    }


    public function paginationView(){
        return 'vendor.livewire.bootstrap';
    }


    /*-----------------------------------Metodo para editar--------------------------------------------------*/

    public function Edit($id){

        $record = Denomination::find($id, ['id', 'type', 'value', 'image']);

        $this->type = $record->type;
        $this->selected_id = $record->id;
        $this->value = $record->value;
        $this->image = null;
      
        $this->emit('show-modal', 'show-modal');
    }


    public function Update(){

        $rules = [
            'type'=> 'required',
            'value'=>'required'
        ];

        $messages = [
            'type.required'=>'El tipo de la moneda es requerido',
            'value.required'=>'El valor de la moneda es requerido'
        ];


        $this->validate($rules, $messages);

        $coin = Denomination::find($this->selected_id);
        $coin->update([
            'type' => $this->type,
            'value' => $this->value
        ]);

        if($this->image){

            $customFileName = uniqid().'_.'.$this->image->extension();

            $this->image->storeAs('public/coins', $customFileName);
            $imageName = $coin->image;
            $coin->image = $customFileName;

            $coin->save();

            if($imageName != null){

                if(file_exists('storage/coins'.$imageName)){
                    unlink('storage/coins'- $imageName);
                }
            }
        }

        $this->resetUI();
        $this->emit('coin-updated', 'Moneda actualizada');


    }



    /*-----------------------------------Fin metodo--------------------------------------------------*/


    /*------------------------------------Metodo para crear------------------------------------------*/

    public function Store(){

        //Definimos reglas de validacion
        $rules = [
            'type'=>'required',
            'value'=>'required'
        ];

        $messages = [
            'type.required' => 'El tipo de moneda es requerido',
            'value.required' => 'El valor de la moneda es requerido'
        ];

        $this->validate($rules, $messages);

        $coin = Denomination::create([
            'type' => $this->type,
            'value' => $this->value
        ]);


        //Variable de nombre de la imagen
        $customFileName;
        if($this->image){

            $customFileName = uniqid().'_.'.$this->image->extension();
            $this->image->storeAs('public/coins',$customFileName);
            $coin->image = $customFileName;
            $coin->save();
        }

        //Reseteamos campos
        $this->resetUI();
        $this->emit('coin-added', 'Moneda registrado');

    }

    /*-----------------------------------Fin metodo--------------------------------------------------*/


     /*------------------------------------Metodo para eliminar------------------------------------------*/

     protected $listeners = ['deleteRow' => 'Destroy'];

     public function Destroy(Denomination $coin){

        $imageName = $coin->image;

        $coin->delete();

        if($imageName != null){
            unlink('storage/coins/'. $imageName);
        }

        $this->resetUI();
        $this->emit('coin-deleted', 'Moneda eliminada');

     }

     /*-----------------------------------Fin metodo--------------------------------------------------*/
    public function resetUI(){

        $this->type = '';
        $this->value = '';
        $this->image = '';
        $this->selected_id = 0;
        $this->emit('modal-hide', 'modal-hide');
    }


}
