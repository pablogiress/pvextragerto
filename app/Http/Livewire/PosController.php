<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PosController extends Component
{

    public $total=10, $cart=[], $itemsQuantity=1;

    public function render()
    {
        return view('livewire.pos.pos')->extends('layouts.theme.app');
    }
}
