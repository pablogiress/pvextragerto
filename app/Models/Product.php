<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'barcode', 'cost', 'price', 'stock', 'alerts', 'image', 'category_id'];

    //Metodo de asociacion de producto a categoria
    public function category(){

        return $this->belongsTo(Category::class);
    }

    public function getImagenAttribute(){

        //Mostramos imagen no valida
        if($this->image == null){
            return 'noimg.gif';
        }

        //Validamos si existe imagen
        if(file_exists('storage/products/'.$this->image)){
            return $this->image;
        }else{
            return 'noimg.gif';
        }
    }


}
