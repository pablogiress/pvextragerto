</div>
      <div class="modal-footer">
        <button type="button" wire:click.prevent="resetUI()" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

        @if($selected_id < 1)
          <button type="button" wire:click.prevent="Store()" class=" btn btn-success close-modal">Guardar</button>
        @else
          <button type="button" wire:click.prevent="Update()" class="btn btn-success close-modal">Actualizar</button>  
        @endif
      </div>
    </div>
  </div>
</div>