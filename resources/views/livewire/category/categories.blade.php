<div>
<div class="row sales layout-top-spacing">
    
    <div class="col-sm-12">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h4 class="h3 mb-3">
                    <b>{{$componentName}} | {{$pageTitle}}</b>
                </h4>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a href="javascript:void(0)" class = "btn" data-toggle="modal" data-target="#theModal" role="button" style="background-color:#74B953; color: white">Agregar</a>
                    </li>
                </ul>

            </div>
            @include('common.searchbox')

            <div class="widget-content">
                    
                <div class="table-responsive">
                    <table class="table table-bordered striped mt-1">
                        <thead class="text-white" style="background: #495057" >
                            <tr>
                                <th class="table-th text-white">Descripción</th>
                                <th class="table-th text-white">Imagen</th>
                                <th class="table-th text-white">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td><h6>{{$category->name}}</h6></td>
                                <td class="text-center">
                                    <span>
                                        <img src="{{ asset('storage/categories/'.$category->imagen) }}" height="70" width="80" class="rounded">
                                    </span>
                                </td>
                                <td class="text-center" style="width:140px">
                                    <a href="javascript:void(0)" wire:click = "Edit('{{$category->id}}')" class="btn mtmobile" title="Edit">
                                     <i class="fas fa-edit"></i>
                                    </a>

                                    <a href="javascript:void(0)" onclick="Confirm('{{$category->id}}',' {{$category->products->count()}}')" class="btn" title="Delete">
                                     <i class="fas fa-trash"></i>
                                    </a>  
                                       
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                    {{$categories->links()}}
                </div>


            </div>
        </div> 
    </div>
    @include('livewire.category.form')
</div>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){


         window.livewire.on('show-modal', msg=>{
            $('#theModal').modal('show')
        });

         window.livewire.on('category-added', msg=>{
            $('#theModal').modal('hide')
        });

        window.livewire.on('category-updated', msg=>{
            $('#theModal').modal('hide')
        });

        window.livewire.on('category-deleted', msg=>{
            $('#theModal').modal('hide')
        });

        window.livewire.on('modal-hide', msg=>{
            $('#theModal').modal('hide')
        });
        
    });


    function Confirm(id, products){

        if(products > 0){
            
            swal({
              type: 'error',
              title: '<i class="fa-solid fa-boxes-stacked"></i> <p style="font-size:15px">No puedes eliminar esta categoria ya que tiene asociado productos</p>',
              confirmButtonColor: '#74B953',
              confirmButtonText: 'Aceptar'
            })
            return;

        }

            swal({
                title: '<p style="font-size:15px">¿Seguro que deseas eliminar la categoria?</p>',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#fff',
                confirmButtonColor: '#74B953',
                confirmButtonText: 'Confirmar'
            }).then(function(result){
                if(result.value){
                    window.livewire.emit('deleteRow', id);
                    swal.close();
                }
            });

    }
</script>

</div>