<div class="row">
	<div class="col-sm-12">
		<div>
			<div class="connect-sorting">
				<h6 class="text-center mb-3">Resumen de venta</h6>
				<div class="connect-sorting-content">
					<div class="card simple-title-task ui-sortable-handle">
						<div class="card-body">
							<div class="task-header">
								<div>
									<h4>Total: ${{number_format($total, 2)}}</h4>
									<input type="hidden" id="hiddenTotal" value="{{$total}}">
								</div>
								<div>
									<h5 class="mb-3">Articulos: {{$itemsQuantity}}</h5>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>