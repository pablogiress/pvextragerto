<div class="connect-sorting">

	<div class="connect-sorting-content">
		<div class="card simple-title-task ui-sortable-handle">
			<div class="card-body">

				@if($total > 0)
				<div class="table-responsive" style="max-height: 650px; overflow: hidden;">
					<table class="table table-striped rounded">
						<thead class="text-white" style="background: #343a40;">
							<tr>
								<th width="10%"></th>
								<th class="table-th text-left text-white">Descripcion</th>
								<th class="table-th text-center text-white">Precio</th>
								<th width="13%" class="table-th text-center text-white">Cant</th>
								<th class="table-th text-center text-white">Importe</th>
								<th class="table-th text-center text-white">Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cart as $item)
							<tr>
								<td class="text-center table-th">
									@if(count($item->attributes) > 0 )
									<span>
										<img src="{{asset('storage/products/'. $item->attributes[0])}}" height="auto" width="90" class="rounded">
									</span>
									@endif
								</td>
								<td><p>{{$item->name}}</p></td>
								<td class="text-center"><p>${{number_format($item->price, 2)}}</p></td>
								<td>
									<input type="number" id="r{{$item->id}}" wire:change="updateQty({{$item->id}}, $('#r' + {{$item->id}}).value() )" style="font-size:1rem!important" class="form-control text-center" value="{{$item->quantity}}">
								</td>
								<td class="text-center">
									<p>
										${{number_format($item->price * $item->quatity, 2)}}
									</p>
								</td>
								<td class="text-center">
									<button onclick="Confirm('{{$item->id}}', 'removeItem', '¿Deseas eliminar el registro?')" class="btn mbmobile">
										<i class="fas- fa-trash-lt"></i>
									</button>

									<button wire:click.prevent="decreaseQty({{$item->id}})" class="btn mbmobile">
										<i class="fas- fa-minus"></i>
									</button>

									<button wire:click.prevent="increaseQty({{$item->id}})" class="btn mbmobile">
										<i class="fas- fa-plus"></i>
									</button>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				@else
				<p class="text-center text-muted rounded">Agrega productos a la venta</p>
				@endif

				<div wire:loading.inline wire:target="saveSale">
					<div class="alert alert-warning" role="alert">
					  Guardando Ticket...
					</div>
				</div>

			</div>
		</div>
	</div>
	
</div>