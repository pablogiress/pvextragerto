<div>
    <style type="text/css">
        
    </style>



    <div class="row layout-top-space">
        <div class="col-sm-12 col-md-8">
            <!--Detalles-->
            @include('livewire.pos.partials.detail')
        </div>
        
        <div class="col-sm-12 col-md-4">
                <!--Totales-->
                @include('livewire.pos.partials.total')


                <!--Monedas-->
                @include('livewire.pos.partials.coins')
        </div>
        

    </div>



</div>

<script type="text/javascript">
    
</script>
