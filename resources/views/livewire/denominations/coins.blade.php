<div>
<div class="row sales layout-top-spacing">
    
    <div class="col-sm-12">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h4 class="h3 mb-3">
                    <b>{{$componentName}} | {{$pageTitle}}</b>
                </h4>
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a href="javascript:void(0)" class = "btn" data-toggle="modal" data-target="#theModal" role="button" style="background-color:#74B953; color: white">Agregar</a>
                    </li>
                </ul>

            </div>
            @include('common.searchbox')

            <div class="widget-content">
                    
                <div class="table-responsive">
                    <table class="table table-bordered striped mt-1">
                        <thead class="text-white" style="background: #495057" >
                            <tr>
                                <th class="table-th text-white">Tipo</th>
                                <th class="table-th text-white">Valor</th>
                                <th class="table-th text-white">Imagen</th>
                                <th class="table-th text-white">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($coins as $coin)
                            <tr>
                                <td><p>{{$coin->type}}</p></td>
                                <td><p>${{number_format($coin->value, 2,',','.')}}</p></td>
                                <td class="text-center">
                                    <span>
                                        <img src="{{ asset('storage/coins/'.$coin->imagen) }}" height="70" width="auto" class="rounded">
                                    </span>
                                </td>
                                <td class="text-center" style="width:140px">
                                    <a href="javascript:void(0)" wire:click = "Edit('{{$coin->id}}')" class="btn mtmobile" title="Edit">
                                     <i class="fas fa-edit"></i>
                                    </a>

                                    <a href="javascript:void(0)" onclick="Confirm('{{$coin->id}}')" class="btn" title="Delete">
                                     <i class="fas fa-trash"></i>
                                    </a>  
                                       
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                    {{$coins->links()}}
                </div>


            </div>
        </div> 
    </div>
    @include('livewire.denominations.form')
</div>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){

         window.livewire.on('coin-added', msg=>{
            $('#theModal').modal('hide');

            Swal.fire({
              position: 'center',
              type: 'success',
              title: 'Moneda insertada',
              showConfirmButton: false,
              timer: 1500
            })
        });

        window.livewire.on('coin-updated', msg=>{
            $('#theModal').modal('hide')
        });

        window.livewire.on('coin-deleted', msg=>{
            //noty
        });

        window.livewire.on('show-modal', msg=>{
            $('#theModal').modal('show')
        });

        window.livewire.on('modal-hide', msg=>{
            $('#theModal').modal('hide')
        });

        window.livewire.on('hidden.bs.modal', msg=>{
            $('.er').css('display', 'none');
        });
        
    });


    function Confirm(id){

        swal({
                title: '<p style="font-size:15px">¿Seguro que deseas eliminar la moneda?</p>',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#fff',
                confirmButtonColor: '#74B953',
                confirmButtonText: 'Confirmar'
            }).then(function(result){
                if(result.value){
                    window.livewire.emit('deleteRow', id);
                    swal.close();
                }
            });

    }



</script>

</div>