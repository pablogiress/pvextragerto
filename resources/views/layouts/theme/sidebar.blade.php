<!--<nav id="compactSidebar">

	<ul class="menu-categories">

		<li class="active">
			<a href="#" class="menu toggle" data-active="true">
				<div class="base-menu">
					<div class="base-icons">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid align-middle me-2"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
					</div>
					<span>Categorias</span>
				</div>
			</a>
		</li>

		<li class="active">
			<a href="#" class="menu toggle" data-active="true">
				<div class="base-menu">
					<div class="base-icons">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box align-middle me-2"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
					</div>
					<span>Productos</span>
				</div>
			</a>
		</li>

		<li class="">
			<a href="#" class="menu toggle" data-active="false">
				<div class="base-menu">
					<div class="base-icons">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign align-middle me-2"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
					</div>
					<span>Ventas</span>
				</div>
			</a>
		</li>

		<li class="">
			<a href="#" class="menu toggle" data-active="false">
				<div class="base-menu">
					<div class="base-icons">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-book align-middle me-2"><path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path><path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path></svg>
					</div>
					<span>Categorias</span>
				</div>
			</a>
		</li>
		
	</ul>-->
<!--</nav>-->

<nav id="sidebar" class="sidebar js-sidebar">
			<div class="sidebar-content js-simplebar">
				<a class="sidebar-brand" href="index.html">
          			<span class="align-middle">POS</span>
        		</a>

				<ul class="sidebar-nav">
					<li class="sidebar-header">
						Almacén
					</li>

					<li class="sidebar-item active">
						<a class="sidebar-link" href="{{url('categories')}}">
              <i class="align-middle" data-feather="list"></i> <span class="align-middle">Categorias</span>
            </a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('products')}}">
              <i class="align-middle" data-feather="box"></i> <span class="align-middle">Productos</span>
            </a>
					</li>

					</li>

					<li class="sidebar-header">
						Ventas
					</li>
					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('pos')}}">
              				<i class="align-middle" data-feather="dollar-sign"></i> <span class="align-middle">Punto de venta</span>
           		 		</a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="{{url('coins')}}">
              				<i class="align-middle" data-feather="tag"></i> <span class="align-middle">Monedas</span>
           		 		</a>
					</li>

					<li class="sidebar-header">
						Informes
					</li>
					<li class="sidebar-item">
						<a class="sidebar-link" href="pages-sign-in.html">
              				<i class="align-middle" data-feather="layers"></i> <span class="align-middle">Arqueos</span>
           		 		</a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="pages-sign-in.html">
              				<i class="align-middle" data-feather="pie-chart"></i> <span class="align-middle">Generales</span>
           		 		</a>
					</li>


					<li class="sidebar-header">
						Seguridad
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="charts-chartjs.html">
              <i class="align-middle" data-feather="grid"></i> <span class="align-middle">Roles</span>
            </a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="maps-google.html">
              <i class="align-middle" data-feather="lock"></i> <span class="align-middle">Permisos</span>
            			</a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="maps-google.html">
              			<i class="align-middle" data-feather="check-square"></i> <span class="align-middle">Asignar</span>
            			</a>
					</li>

					<li class="sidebar-item">
						<a class="sidebar-link" href="maps-google.html">
              			<i class="align-middle" data-feather="users"></i> <span class="align-middle">Usuarios</span>
            			</a>
					</li>
				</ul>

				<!--<div class="sidebar-cta">
					<div class="sidebar-cta-content">
						<strong class="d-inline-block mb-2">Upgrade to Pro</strong>
						<div class="mb-3 text-sm">
							Are you looking for more components? Check out our premium version.
						</div>
						<div class="d-grid">
							<a href="upgrade-to-pro.html" class="btn btn-primary">Upgrade to Pro</a>
						</div>
					</div>
				</div>-->
			</div>
</nav>