<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="{{ asset('img/icons/icon-48x48.png') }}" />

	<title>Punto de venta Extragerto</title>

	<!--ESTYLE CSS-->
	@include('layouts.theme.styles')
	<!--END ESTYLE CSS-->
</head>

<body>

	<div class="wrapper">


		<!--SIDE BAR BARRA LATERAL-->
		@include('layouts.theme.sidebar')
		<!--END SIDE BAR BARRA LATERAL-->
		
		<div class="main"> 

			<!--NAV BAR ARRIBA-->
			@include('layouts.theme.header')
			<!-- END NAV BAR ARRIBA-->


			 <!--DASHBOARD CONTENT-->
			<main class="content">
				
				<div class="container-fluid p-0">

					@yield('content')


				</div>
			</main>
			<!-- END DASHBOARD CONTENT-->

			<!--FOOTER-->
			@include('layouts.theme.footer')
			<!--END FOOTER-->
		</div>
	</div>
	<!--SCRIPTS-->
	@include('layouts.theme.scripts')
	<!--END SCRIPTS-->

</body>

</html>