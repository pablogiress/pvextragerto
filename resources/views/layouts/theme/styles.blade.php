<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="{{ asset('plugins/sweetalerts/sweetalert.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/font-icons/fontawesome/css/fontawesome.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('plugins/notification/snackbar/snackbar.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ asset('plugins/sweetalerts/sweetalert.css') }}" >

<link rel="stylesheet" href="{{ asset('css/scrumboard.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ asset('css/notes.css') }}" rel="stylesheet" type="text/css">

<style>
	aside{
		display: none!important;
	}

	.page-item.active .page-link{
		z-index: 3;
		color: #fff;
		background-color: #3b3f5c;
		border-color: #3b3f5c;
	}

	@media(max-width: 480px){
		.mtmobile{
			margin-bottom: 20px!important;
		}

		.mbmobile{
			margin-bottom: 10px!important;
		}

		.hideonsm{
			display: none!important;
		}

		.inblock{
			display: block;
		}
	}

	body{
		font-family: 'Nunito Sans', sans-serif;
	}

	form{
		font-family: 'Nunito Sans', sans-serif;
	}
</style>

<!--Cambiamos estilo de la barra de scroll-->
<style>
    
     /* width */
::-webkit-scrollbar {
  width: 5px;
}

::-webkit-scrollbar-track {
  border-radius: 10px;
}


/* Handle */
::-webkit-scrollbar-thumb {
  background: #212529;
      border-radius: 10px;
} 
</style>

@livewireStyles