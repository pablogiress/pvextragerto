<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([

            'name' => 'Cursos',
            'image' => ''
        ]);

        Category::create([

            'name' => 'Ropa',
            'image' => ''
        ]);

        Category::create([

            'name' => 'Celulares',
            'image' => ''
        ]);

        Category::create([

            'name' => 'Varios',
            'image' => ''
        ]);
    }
}
