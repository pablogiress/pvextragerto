<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([

            'name' => 'CURSO LARAVEL',
            'cost' => 200000,
            'price' => 300000,
            'barcode' => '0235',
            'stock' => 500,
            'alerts' => 10,
            'category_id' => 1,
            'image' => 'curso.png'
        ]);


        Product::create([

            'name' => 'SACO NEGRO CON LOGO DE LARAVEL',
            'cost' => 50000,
            'price' => 80000,
            'barcode' => '0236',
            'stock' => 20,
            'alerts' => 5,
            'category_id' => 2,
            'image' => 'curso.png'
        ]);

        Product::create([

            'name' => 'CELULAR IPHONE',
            'cost' => 2300000,
            'price' => 3000000,
            'barcode' => '0237',
            'stock' => 10,
            'alerts' => 5,
            'category_id' => 3,
            'image' => 'curso.png'
        ]);

        Product::create([

            'name' => 'STICKERS PROGRAMACION',
            'cost' => 3000,
            'price' => 5500,
            'barcode' => '0238',
            'stock' => 50,
            'alerts' => 20,
            'category_id' => 4,
            'image' => 'curso.png'
        ]);
    }
}
