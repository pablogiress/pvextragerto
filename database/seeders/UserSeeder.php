<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

            'name' => 'Diana Tejedor',
            'phone' => '3112436393',
            'email' => 'dianatejedor318@gmail.com',
            'password' => bcrypt('diana123'),
            'profile' => 'ADMIN',
            'status' => 'ACTIVE'
        ]);

        User::create([

            'name' => 'Soporte',
            'phone' => '3112436393',
            'email' => 'soporte@tecnokli.com',
            'password' => bcrypt('soporte123'),
            'profile' => 'EMPLOYEE',
            'status' => 'ACTIVE'
        ]);
    }
}
